const Stock = require('./stock');

class Cart {
    constructor() {
        this.products = [];
        this.total = 0;
    }
    async addProduct(db, product_desc) {
        try {
            
            var product = await db.collection("products").findOne({desc: product_desc});
            Stock.comproveStock(product);
            this.products.push(product);
            this.total += product.price; 

        } catch (err) {
            console.error(err);
        }
    }

    async removeProduct(product_desc) { 

        try {

            this.products.forEach(product => {

                if(product.desc == product_desc) { 
                    
                    this.products.splice(this.products.indexOf(product), 1); 
                    this.total -= product.price;
                    return
                }
            }); 

        } catch (error) {
            console.log(error);
        }
        
    }

    toString() {
        var res = "Shopping Cart contains "  + this.products.length + " product/s:\n";
        
        this.products.forEach(product => {
            res += "- "  + product.desc + "\n";
        });

        res += "for a total of " + this.total + " $"
        return res;
    }
}

module.exports = Cart;