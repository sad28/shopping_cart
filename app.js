const { MongoClient } = require('mongodb');
const Cart = require('./cart'); 
require('dotenv').config();


async function main() {

    /**
     * Connection URI. Update <username>, <password>, and <your-cluster-url> to reflect your cluster.
     * See https://docs.mongodb.com/ecosystem/drivers/node/ for more details
     */

    const uri = "mongodb+srv://" + process.env.DB_USER + ":" + process.env.DB_PASSWORD + "@cluster0.lsuet.mongodb.net/shopping_cart?retryWrites=true&w=majority"; 
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

    try {

        // Connect to the MongoDB cluster
        await client.connect();

        // Make the appropriate DB calls
        const db = client.db();

        // Create Cart
        var cart = new Cart(); 

        /*  naranjas
            manzanas
            peras
            fresas
            uvas -sin stock- */ 

        console.log("-------------- Se añaden los productos ------------------");

        await cart.addProduct(db, "manzanas"); 
        await cart.addProduct(db, "peras");
        await cart.addProduct(db, "fresas");
        await cart.addProduct(db, "peras"); 
        console.log(cart.toString()); 

        console.log("\n-------------- Se elimina el producto ------------------");

        await cart.removeProduct("manzanas"); 
        console.log(cart.toString()); 
        await cart.addProduct(db, "uvas"); 


    } catch (err) {
        console.error(err);
    } finally {                   
        await client.close();
    } 
}

main().catch(console.error);