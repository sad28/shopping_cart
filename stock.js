
class Stock {
    constructor() { }
    
    static comproveStock(product) {

        if(product.stock == 0){
            throw new Error("No hay stock del producto " + product.desc);
        }else{
            console.log("Hay stock del producto " + product.desc);
        }
    }
}
module.exports = Stock;