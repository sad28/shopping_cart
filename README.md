# shopping-cart

## Authors and acknowledgment  

- Adrian Muñoz Felder   
- Maria Ortiz Riera 

## Usage
open terminal on prent directory and type:

```
vagrant up
```

enter virtual vagrant dev environment with:

```
vagrant ssh
```

move to /vagrant on virtual machine and execute 

```
npm install
```

this way npm packages will be retrived from package.json and installed  

create a .env file on parent directory and add:

```
DB_USER=<mongo_user> 
DB_PASSWORD=<mongodb_password>
```

execute on /vagrant 
```
node app.js
```

## Support

email at admuofel@inf.upv.es 
or at mortrie@etsinf.upv.es 

## Explication

Hemos utilizado la cláusula await para las operaciones de consulta a la BBDD que devolvían promesas y la cláusula async para las funciones con operaciones asíncronas.  
Utilizamos find_one para las consultas a la BBDD que devuelve el primer elemneto de la búsqueda que coincide y esta búsqueda la realizamos por el elemento desc ya que así es más legible el programa y este es único. 
Se ha desarrollado dos módulos uno cart para las operaciones del carrito dónde se ha sobrescrito el método toString(). El método addProduct que require de un objeto de BBDD de esta forma creo que es más elegante que importando el módulo de mongodb directamente en el fichero cart, también requiere de la descripción del producto a añadir. Por  último el método removeProduct necesita únicamente la descripción del producto que utiliza para eliminar del carrito local la primera aparición del producto que sea igual. En este método se decidió implementar de esta manera debido a que volver a recuperar el mismo producto con una consulta y utilizarlo para comparar con los productos añadidos ya al carrito haciendo uso del método indexOf retornava -1 al no ser ambos productos idénticos.

## Añadir y eliminar productos
Primeramente se ha generado el fichero cart, siendo esta la clase del carrito donde se pueden realizar las diferentes operaciones como añadir y eliminar productos del carrito. Además, se cuenta con la función toString para mostrar la información de los productos que hay en el carrito.

## Ejemplo
-------------- Se añaden los productos ------------------
Shopping Cart contains 4 product/s:
- manzanas
- peras
- fresas
- peras
for a total of 12 $

-------------- Se elimina el producto manzanas ------------------
Shopping Cart contains 3 product/s:
- peras
- fresas
- peras
for a total of 10 $

## Comprobar stock
Una vez comprobado que los elementos se añaden y se borran del carrito, se ha procedido a comprobar mediante el módulo "ComproveStock" si un producto tiene stock o no. Si un producto no tiene stock se indicará mediante una excepción, mientras que si todos los productos añadidos tienen stock, se realizará sin problemas la carga de datos.

## Ejemplo
-------------- Se añaden los productos ------------------
Hay stock del producto peras
Shopping Cart contains 4 product/s:
- manzanas
- peras
- fresas
- peras
for a total of 12 $

-------------- Se elimina el producto manzanas ------------------
Shopping Cart contains 3 product/s:
- peras
- fresas
- peras
for a total of 10 $
Error: No hay stock del producto uvas
    at Stock.ComproveStock (/vagrant/ComproveStock.js:34:19)
    at main (/vagrant/app.js:66:14)
    at processTicksAndRejections (internal/process/task_queues.js:97:5) 

## Description

Objetivo:

Diseñar e implementar un módulo en javascript que implemente la funcionalidad de
un carrito de la compra. Dicho módulo permitirá realizar operaciones básicas: 
añadir producto a carro, quitar producto de carro, que se traduzca su contenido a un 
string (toString)  

Paso 1.   
Pensar cuidadosamente cómo usar las herramientas proporcionadas por Nodejs 
para realizar una implementación adecuada. El carrito será creado como una clase 
con las operaciones descritas.  

Paso 2.  
Realizar un script Nodejs separado código para mostrar el funcionamiento 
correcto de este código.  

Paso 3. 
Diseñar un código usado desde un módulo que nos permitirá realizar una 
operación de consulta sobre un servicio con mongodb. De forma sencilla debe 
comprobar si existe o no stock del producto indicado. En caso de no existir stock, se 
debe devolver el estado de error (en otro caso se indica simplemente que sí hay 
stock).  

Paso 4.   
Realizar un script Nodejs separado código para mostrar el funcionamiento 
correcto de este código.  

RECURSOS:
- Ejemplo de código para la creación de colecciones y consulta a una base de datos 
mongodb (Cliente_mongo.js )  
- Máquina vagrant con mongodb instalado (Maquina Vagrant con mongodb  )
- Máquina vagrant con nodejs, zmq, y mongodb instalado (  Maquina Vagrant con node, 
zmq, y mongodb 

## URL del repositorio
https://gitlab.com/sad28/shopping_cart